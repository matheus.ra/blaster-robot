#Blast Robot -
O Blast foi feito com o conceito de um tanque de parede, ele não fica no meio do campo de batalha(a não sei nos momentos de transição).

#Pontos Fortes/Francos   - 
Por ser um tanque de parede, o Blasnt evita bastante o fogo cruzado do meio da arena, mantendo-o em uma posição segura em relação ao centro, ele também está quase sempre em movimento,com exceção de uma caso, onde Blast encontra outro tanque parado em sua frente, nessa condição ele irá parar de se movimentar e atira repetidas vezes no alvo, porem em outras situações ele sempre estará em movimento o que evita que ele seja atingido muitas vezes em sequência por tanques estáticos.  

Por outro lado o Blast não possui resposta quanto sofre dano por disparos, pois dessa forma ele ficaria muito tempo se reposicionando e pouco tempo efetivamente na estratégia de parede. 

#Logica   -
No começo do combate a prioridade do Blast é encontrar uma parede, uma vez que ele a encontra, começara o ciclo de dar voltas rente a parede e efetuar disparos quando um inimigo for detectado, duas coisas podem acontecer durante esse processo, a primeira sendo caso ele encontre outro tanque parado em sua frente, ele irá parar de se movimentar e atirar repetidas vezes no alvo, a outro situação será caso algum tanque colida com o Blast, dessa forma ele irá se movimentar de uma forma diferente em busca de uma nova parede para continuar o ciclo  

#Desafios//Aprendizados  -
Durante essa fase eu pude conhecer uma maneira diferente e empolgante de aprender programação, nunca tive contato com Java antes então foi um certo desafio, porem após um tempo de vídeo aulas e pesquisas vi que o robocode em si era bem amigável, e já nos disponibilizava detalhadamente o uso e manipulação de cada função e variável. Percebi também que o robocode possui uma comunidade muito grande e ativa, com diversos tipos de tanques e estratégias bem avançadas 