package blaster;
import robocode.HitRobotEvent;
import robocode.Robot;
import robocode.ScannedRobotEvent;
import java.awt.Color;

public class Blaster extends Robot
{
	boolean olhar;  //Verificar antes de mudar//
	double moverMAX; 
	

	public void run() {
		
		setColors(Color.white,Color.red,Color.white);
	
		moverMAX= Math.max(getBattleFieldWidth(), getBattleFieldHeight());
		
		olhar = false;
		
		turnLeft(getHeading() % 90);
		ahead(moverMAX);
		
		olhar = true;
		turnGunRight(90);
		turnRight(90);

		while (true) {
			
			olhar = true;
		
			ahead(moverMAX);
		
			olhar = false;
		
			turnRight(90);

}}

	public void onHitRobot(HitRobotEvent e) {
	
		if (e.getBearing() > -90 && e.getBearing() < 90) {
			back(100);
		}
		else {
			ahead(100);
}}
		
	public void onScannedRobot(ScannedRobotEvent e) {
		fire(3);
		if (olhar){scan();}
 }}
